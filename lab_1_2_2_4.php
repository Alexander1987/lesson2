<?php

$text = 'apple';

echo 'Simple single-quoted string with variable by $text' . PHP_EOL;
echo 'Single-quoted string with characters escaping by \'' . $text . '\'' . PHP_EOL;
echo "Simple double-quoted string with variable by {$text}" . PHP_EOL;
echo "Double-quoted string with escape sequences\nby '{$text}'" . PHP_EOL;

echo PHP_EOL;
echo <<<HEREDOC
everybody
like to
eat "{$text}"
HEREDOC;

echo PHP_EOL;
echo <<<'NOWDOC'
everybody
like to
eat "{$text}"
NOWDOC;


