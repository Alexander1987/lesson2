<?php

// меняем типі переменных

echo 'Integer "5" to boolean: ';
var_dump((bool)5);
echo 'Integer "0" to boolean: ';
var_dump((bool)0);
echo PHP_EOL;

echo 'Boolean "true" to integer: ';
var_dump((int)true);
echo 'Boolean "false" to integer: ';
var_dump((int)false);
echo PHP_EOL;

echo 'Float "1,28" to integer: ';
var_dump((int)1.28);
echo PHP_EOL;

echo 'String "first line" to integer: ';
var_dump((int)'first line');
echo 'String "5 apples" to integer: ';
var_dump((int)'5 apples');
echo PHP_EOL;

echo 'String "first line" to array: ';
var_dump((array)'first line');
echo 'Array "[3,5,4]" to string: ';
var_dump((string)[3,5,4]);
echo 'Array "[3,4,5]" to object: ';
var_dump((object)[3,4,5]);