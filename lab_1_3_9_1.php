<?php
function calculateFactorial($n)
{
    if(!$n) return 1;
    return $n * calculateFactorial($n-1);
}
echo calculateFactorial(0)."\n";//1
echo calculateFactorial(1)."\n";//1
echo calculateFactorial(4)."\n";//24