<?php


// инициализация переменных

$integer = 5;
echo 'Integer is: ';
var_dump($integer); //показівает тип и значение

$float = 1.28;
echo 'Float is: ';
var_dump($float);

$string = 'Its all string';
echo 'String is: ';
var_dump($string);

$boolean = true;
echo 'Boolean is: ';
var_dump($boolean);

$array = [5, 4, 3];
echo 'Array is: ';
var_dump($array);

$object = new stdClass();
echo 'Object is: ';
var_dump($object);

$resource = curl_init();
echo 'Resource is: ';
var_dump($resource);
curl_close($resource);

$null = null;
echo 'Null is: ';
var_dump($null);
