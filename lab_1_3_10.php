<?php

$start = '--------------------';

function drawDelimiter()
{
    global $start;
    echo $start;
}

$start2 = 'Nice day';

$drawContentString = function () use ($start2) {
    echo $start2;
};

drawDelimiter();
echo "\n";
$drawContentString();
echo "\n";
drawDelimiter();
