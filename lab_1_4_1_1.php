<?php
//var_dump($argv);

function calculator($number1,$operation,$number2)
{
    if (empty($operation))
    {
        echo "Error: Empty operation";
        return false;
    }



    if (!is_null($number1) || !is_null($number2))
    {
        echo "Error: Bad operation";
        return false;
    }

    switch ($operation)
    {
        case '+':
            return $number1 + $number2;

        case '-':
            return $number1 - $number2;

        case '*':
            return $number1 * $number2;

        case '/':
            return $number1 / $number2;

        default:
            echo "Error: Bad operation";
            return false;
    }
}

$number1= isset($argv[1]) ? $argv[1]:0;
$number2= isset($argv[3]) ? $argv[3]:0;
$operation= isset($argv[2]) ? $argv[2]:'';

$result= calculator($number1,$operation,$number2);
//операция число = результат 1+2

if($result === false)
{
    exit;
}
echo "{$number1}"."{$operation}"."{$number2} = "."{$result}";




